package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    public static void main(String[] args) throws Exception {

    	initializeQuestions();
    	
        SpringApplication.run(Application.class, args);
    }
    
    private static void initializeQuestions() {
		QuestionWithAnswer q1 = new QuestionWithAnswer("1", "1", "1");
    	QuestionWithAnswer q2 = new QuestionWithAnswer("1", "1", "2");
    	QuestionWithAnswer q3 = new QuestionWithAnswer("1", "2", "1");
    	QuestionWithAnswer q4 = new QuestionWithAnswer("1", "2", "2");
    	QuestionWithAnswer q5 = new QuestionWithAnswer("1", "3", "1");
    	QuestionWithAnswer q6 = new QuestionWithAnswer("1", "3", "2");
    	QuestionWithAnswer q7 = new QuestionWithAnswer("2", "1", "1");
    	QuestionWithAnswer q8 = new QuestionWithAnswer("2", "1", "2");
    	QuestionWithAnswer q9 = new QuestionWithAnswer("2", "2", "1");
    	QuestionWithAnswer q10 = new QuestionWithAnswer("2", "2", "2");
    	QuestionWithAnswer q11 = new QuestionWithAnswer("2", "3", "1");
    	QuestionWithAnswer q12 = new QuestionWithAnswer("2", "3", "2");
    	
    	Result result = Result.getInstance();
    	
    	result.addQuestion(q1);
    	result.addQuestion(q2);
    	result.addQuestion(q3);
    	result.addQuestion(q4);
    	result.addQuestion(q5);
    	result.addQuestion(q6);
    	result.addQuestion(q7);
    	result.addQuestion(q8);
    	result.addQuestion(q9);
    	result.addQuestion(q10);
    	result.addQuestion(q11);
    	result.addQuestion(q12);
	}
}

/*@SpringBootApplication
public class Application {

    public static void main(String[] args) {
    	
    	initializeQuestions();
    	
        SpringApplication.run(Application.class, args);
    }

	private static void initializeQuestions() {
		QuestionWithAnswer q1 = new QuestionWithAnswer("1", "1", "1");
    	QuestionWithAnswer q2 = new QuestionWithAnswer("1", "1", "2");
    	QuestionWithAnswer q3 = new QuestionWithAnswer("1", "2", "1");
    	QuestionWithAnswer q4 = new QuestionWithAnswer("1", "2", "2");
    	QuestionWithAnswer q5 = new QuestionWithAnswer("1", "3", "1");
    	QuestionWithAnswer q6 = new QuestionWithAnswer("1", "3", "2");
    	QuestionWithAnswer q7 = new QuestionWithAnswer("2", "1", "1");
    	QuestionWithAnswer q8 = new QuestionWithAnswer("2", "1", "2");
    	QuestionWithAnswer q9 = new QuestionWithAnswer("2", "2", "1");
    	QuestionWithAnswer q10 = new QuestionWithAnswer("2", "2", "2");
    	QuestionWithAnswer q11 = new QuestionWithAnswer("2", "3", "1");
    	QuestionWithAnswer q12 = new QuestionWithAnswer("2", "3", "2");
    	
    	Result result = Result.getInstance();
    	
    	result.addQuestion(q1);
    	result.addQuestion(q2);
    	result.addQuestion(q3);
    	result.addQuestion(q4);
    	result.addQuestion(q5);
    	result.addQuestion(q6);
    	result.addQuestion(q7);
    	result.addQuestion(q8);
    	result.addQuestion(q9);
    	result.addQuestion(q10);
    	result.addQuestion(q11);
    	result.addQuestion(q12);
	}
}*/