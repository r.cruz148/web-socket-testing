package hello;

public class QuestionWithAnswer {
	
	private String section;
	private String part;
	private String question;
	private String answer;
	
	public QuestionWithAnswer() {
	}
	
	public QuestionWithAnswer(String section, String part, String question, String answer) {
		this.section = section;
		this.part = part;
		this.question = question;
		this.answer = answer;
	}

	public QuestionWithAnswer(String section, String part, String question) {
		this(section, part, question, "");
	}

	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getPart() {
		return part;
	}
	public void setPart(String part) {
		this.part = part;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	public String getKey() {
		return this.section + "|" + this.part + "|" + this.question;
	}
	
	@Override
	public String toString() {
		return "Section " + this.section + " | Part " + this.part + " | Question " + this.question + " | Answer " + this.answer;
	}
}
