package hello;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ResultController {

	@MessageMapping("/answer")
	@SendTo("/topic/greetings")
	public Result results(QuestionWithAnswer answer) throws Exception {
		Result.getInstance().updateQuestion(answer);
		return Result.getInstance();
	}
	
//	@RequestMapping(value={"/question"})
//	public String question() {
//		return "question.html";
//	}
}