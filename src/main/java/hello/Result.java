package hello;

import java.util.HashMap;

public class Result {

	private HashMap<String, HashMap> sections;
	private static Result myInstance = new Result();
	
	private Result() {
		sections = new HashMap<String, HashMap>();
//		initialize();
	}
	
	public static Result getInstance() {
		return myInstance;
	}
	
	public void addQuestion(QuestionWithAnswer question) {
		getQuestionsMap(question).put(question.getQuestion(), question.getAnswer());
	}
	
	private HashMap<String, String> getQuestionsMap(QuestionWithAnswer question) {
		HashMap<String, HashMap> parts = sections.get(question.getSection());
		
//		if (section == null) {
//			section = new HashMap<String, HashMap>();
//			sections.put(question.getSection(), section);
//		}
//		HashMap<String, HashMap> part = (HashMap<String, HashMap>) section.get(question.getPart());
		
		if (parts == null) {
			parts = new HashMap<String, HashMap>();
			sections.put(question.getSection(), parts);
		}
		HashMap<String, String> questions = (HashMap<String, String>) parts.get(question.getPart());
		
		if (questions == null) {
			questions = new HashMap<String, String>();
			parts.put(question.getPart(), questions);
		}
		
		return questions;
	}

	public void updateQuestion(QuestionWithAnswer question) {
		//addQuestion(question);
		try {
			HashMap<String, HashMap> parts = sections.get(question.getSection());
			HashMap<String, String> questions = (HashMap<String, String>) parts.get(question.getPart());
			questions.put(question.getQuestion(), question.getAnswer());
		} catch (NullPointerException e) {
			System.err.println("Section | Part | Question not exists...");
		}
	}
	
	public HashMap<String, HashMap> getSections() {
		return sections;
	}
	
	private void initialize() {
		sections.put("1", initParts());
		sections.put("2", initParts());
	}

	private HashMap<String, HashMap> initParts() {
		HashMap<String, HashMap> parts;
		parts = new HashMap<String, HashMap>();
		parts.put("1", initQuestions());
		parts.put("2", initQuestions());
		parts.put("3", initQuestions());
		return parts;
	}

	private HashMap<String, String> initQuestions() {
		HashMap<String, String> questions;
		questions = new HashMap<String, String>();
		questions.put("1", "");
		questions.put("2", "");
		return questions;
	}
}
