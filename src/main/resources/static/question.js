var stompClient = null;

function connect() {
    var socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
//        setConnected(true);
        console.log('Connected: ' + frame);
//        stompClient.subscribe('/topic/greetings', function (greeting) {
//            showGreeting(JSON.parse(greeting.body).content);
//        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
//    setConnected(false);
    console.log("Disconnected");
}

function sendAnswer() {
    stompClient.send("/app/answer", {},
    		JSON.stringify(
    				{
    					'section': $("#section").val(),
    					'part': $("#part").val(),
    					'question': $("#question").val(),
    					'answer': document.querySelector('input[name="answer"]:checked').value
    				}));
}

$(function () {
	connect();
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#confirm" ).click(function() { sendAnswer(); });
})